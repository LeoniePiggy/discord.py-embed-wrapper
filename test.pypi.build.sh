echo "If this script errors make sure you bump the version in setup.py before running again"
rm -rf build
rm -rf discord.py-embed-wrapper.egg-info
rm -rf dist
python3 setup.py sdist bdist_wheel
twine upload --repository testpypi dist/*